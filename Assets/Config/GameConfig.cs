using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Stage
{
    public int        AmoutKnife;
    public float      PlateSpeed;
    public GameObject PlatePrefab;
}


[CreateAssetMenu(fileName = "GameConfig", menuName = ("Scriptable/GameConfig"))]
public class GameConfig : ScriptableObject
{
    public List<Stage> ListStage;
}
