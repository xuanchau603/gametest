using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Knife
{
    public string Id;
    public Sprite Shadow;
    public Sprite Image;
    public int    Price;
}

[CreateAssetMenu(fileName = "KnifeConfig", menuName = ("Scriptable/KnifeConfig"))]
public class KnifeConfig : ScriptableObject
{
    public List<Knife> ListKnife;
}
