using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ListKnifes : MonoBehaviour
{
    [SerializeField] GameObject  m_root;
    [SerializeField] Button      m_btnBack;
    [SerializeField] Image       m_knifeSelect;
    [SerializeField] GameObject  m_btnSelectKnife;
    [SerializeField] Button      m_btnUnlockKnife;
    [SerializeField] TMP_Text    m_txtAppleUnlock;
    [SerializeField] KnifeConfig m_knifeConfig;
    [SerializeField] Transform   m_spawnListKnife;
    DataWapper                   dataWapper           = new DataWapper();
    List<GameObject>             m_listBtnSelectKnife = new List<GameObject>();

    void Awake()
    {
        m_btnUnlockKnife.onClick.AddListener(BtnUnLockOnClick);
        m_btnBack.onClick.AddListener(BtnBackOnClick);
        LoadListKnife();
    }

    void OnDestroy()
    {
        m_btnUnlockKnife.onClick.RemoveListener(BtnUnLockOnClick);
        m_btnBack.onClick.RemoveListener(BtnBackOnClick);
    }

    public void Show(bool active)
    {
        m_root.SetActive(active);
    }

    void BtnBackOnClick()
    {
        m_root.SetActive(false);
        UIManager.Instance.BeginUI.Show(true);
    }

    void LoadListKnife()
    {
        //Load data from json data
        dataWapper = JsonUtility.FromJson<DataWapper>(PlayerPrefs.GetString(GameConstant.KNIFE_DATA));
        for (int i = 0; i < m_knifeConfig.ListKnife.Count; i++)
        {
            //Init button knife
            GameObject go = Instantiate(m_btnSelectKnife, m_spawnListKnife);
            m_listBtnSelectKnife.Add(go);
            Transform child     = go.transform.GetChild(0);
            Image  imgParent = go.GetComponent<Image>();
            if (child == null)
                continue;
            Image image = child.GetComponent<Image>();
            if (image == null)
                continue;
            Sprite sprite = null;
            //Check if lock show shadow
            if (dataWapper.KnifeDatas[i].IsLock)
            {
                sprite = m_knifeConfig.ListKnife[i].Shadow;
            }
            else
            {
                sprite          = m_knifeConfig.ListKnife[i].Image;
                imgParent.color = Color.gray;
                image.color     = Color.white;
            }
            image.sprite = sprite;
            string Id     = m_knifeConfig.ListKnife[i].Id;
            bool   isLock = dataWapper.KnifeDatas[i].IsLock;
            int    price  = m_knifeConfig.ListKnife[i].Price;
            Button button = go.GetComponent<Button>();
            if (button == null)
                continue;
            button.onClick.AddListener(() => BtnSelectKnifeOnClick(Id, isLock, price));
        }
    }

    void BtnSelectKnifeOnClick(string Id, bool isLock, int price)
    {
        Sprite sprite = null;
        //Handle select knife
        if (!isLock)
        {
            sprite = m_knifeConfig.ListKnife.FirstOrDefault(item => item.Id.Equals(Id))?.Image;
            m_btnUnlockKnife.gameObject.SetActive(false);
            PlayerPrefs.SetString(GameConstant.KNIFE_SKIN, Id);
            m_knifeSelect.color = Color.white;
            Debug.Log("Select Knife");
        }
        else
        {
            m_btnUnlockKnife.gameObject.SetActive(true);
            m_txtAppleUnlock.text = price.ToString();
            PlayerPrefs.SetInt(GameConstant.KNIFE_PRICE_KEY, price);
            sprite              = m_knifeConfig.ListKnife.FirstOrDefault(item => item.Id.Equals(Id))?.Shadow;
            m_knifeSelect.color = Color.gray;
        }
        m_knifeSelect.sprite = sprite;
        PlayerPrefs.SetString(GameConstant.KNIFE_SELECT, Id);
    }

    void BtnUnLockOnClick()
    {
        //Handle unlock knife
        
        int currentApple = PlayerPrefs.GetInt(GameConstant.APPLE_KEY);
        int price        = PlayerPrefs.GetInt(GameConstant.KNIFE_PRICE_KEY);
        if (currentApple < price)
        {
            Debug.Log("Apple is not enough");
            return;
        }
        currentApple -= price;
        for (int i = 0; i < dataWapper.KnifeDatas.Count; i++)
        {
            if (dataWapper.KnifeDatas[i].Id.Equals(PlayerPrefs.GetString(GameConstant.KNIFE_SELECT)))
            {
                dataWapper.KnifeDatas[i].IsLock = false;
                m_knifeSelect.sprite            = m_knifeConfig.ListKnife.FirstOrDefault(item => item.Id.Equals(dataWapper.KnifeDatas[i].Id))?.Image;
                m_knifeSelect.color = Color.white;
                PlayerPrefs.SetString(GameConstant.KNIFE_SKIN, dataWapper.KnifeDatas[i].Id);
                break;
            }
        }
        PlayerPrefs.SetString(GameConstant.KNIFE_DATA, JsonUtility.ToJson(dataWapper));
        PlayerPrefs.SetInt(GameConstant.APPLE_KEY, currentApple);
        ResetListKnife();
        UIManager.Instance.HudUI.UpdateUI();
        m_btnUnlockKnife.gameObject.SetActive(false);
    }

    void ResetListKnife()
    {
        for (int i = 0; i < m_spawnListKnife.childCount; i++)
        {
            Destroy(m_spawnListKnife.GetChild(i).gameObject);
        }
        LoadListKnife();
    }
}