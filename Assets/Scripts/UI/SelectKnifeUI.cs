using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class KnifeData
{
    public string Id;
    public bool   IsLock;
}

[Serializable]
public class DataWapper
{
    public List<KnifeData> KnifeDatas;
}

public class SelectKnifeUI : MonoBehaviour
{
    [SerializeField] KnifeConfig     m_knifeConfig;
    List<KnifeData> DataList = new List<KnifeData>();
    void Awake()
    {
        LoadData();
    }


    void LoadData()
    {
        if (!PlayerPrefs.HasKey(GameConstant.KNIFE_DATA))
        {
            InitData();
        }
    }


    [ContextMenu(nameof(InitData))]
    void InitData()
    {
        for (int i = 0; i < m_knifeConfig.ListKnife.Count; i++)
        {
            KnifeData kf = new KnifeData();
            kf.Id = m_knifeConfig.ListKnife[i].Id;
            if (i == 0)
            {
                kf.IsLock = false;
            }
            else
            {
                kf.IsLock = true;
            }
            DataList.Add(kf);
            PlayerPrefs.SetString(GameConstant.KNIFE_DATA, JsonUtility.ToJson(new DataWapper { KnifeDatas = DataList }));
        }
    }
    
    
}