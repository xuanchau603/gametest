using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayGameUI : MonoBehaviour
{
    [SerializeField] GameObject m_root;
    [SerializeField] TMP_Text   m_txtScore;
    [SerializeField] TMP_Text   m_txtStage;


    public void Show(bool active)
    {
        if (active)
        {
            m_txtScore.text = "0";
            m_txtStage.text = "STAGE 1";
        }
        m_root.SetActive(active);
    }

    public void UpdateScore()
    {
        m_txtScore.text = GameManager.Instance.Score.ToString();
    }

    public void UpdateStage()
    {
        m_txtStage.text = $"STAGE {GameManager.Instance.Stage+1}";
    }

    
}
