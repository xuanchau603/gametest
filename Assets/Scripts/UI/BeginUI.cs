using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BeginUI : MonoBehaviour
{
    [SerializeField] GameObject m_root;
    [SerializeField] Button     m_btnStart;
    [SerializeField] Button     m_btnSelectSkinKnife;

    void Awake()
    {
        m_btnStart.onClick.AddListener(BtnStartOnClick);
        m_btnSelectSkinKnife.onClick.AddListener(BtnSelectSkinKnifeOnClick);
    }

    public void Show(bool active)
    {
        m_root.SetActive(active);
    }

    void OnDisable()
    {
        m_btnStart.onClick.RemoveAllListeners();
    }

    void BtnStartOnClick()
    {
        m_root.SetActive(false);
        UIManager.Instance.PlayGameUI.Show(true);
        SceneManager.LoadScene(GameConstant.STAGE_SCENE);
    }

    void BtnSelectSkinKnifeOnClick()
    {
        m_root.SetActive(false);
        UIManager.Instance.ListKnifes.Show(true);
    }
}
