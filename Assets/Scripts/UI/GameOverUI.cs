using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverUI : MonoBehaviour
{
    [SerializeField] GameObject m_root;
    [SerializeField] TMP_Text   m_txtScore;
    [SerializeField] TMP_Text   m_txtStage;
    [SerializeField] Button     m_btnRestart;
    [SerializeField] Button     m_btnHome;

    void Awake()
    {
        m_btnRestart.onClick.AddListener(BtnRestartOnClick);
        m_btnHome.onClick.AddListener(BtnHomeOnClick);
    }

    void OnDisable()
    {
        m_btnRestart.onClick.RemoveAllListeners();
        m_btnHome.onClick.RemoveAllListeners();
    }

    public void Show(bool active)
    {
        m_txtScore.text = GameManager.Instance.Score.ToString();
        m_txtStage.text = $"STAGE {GameManager.Instance.Stage+1}";
        m_root.SetActive(true);
    }


    void BtnRestartOnClick()
    {
        m_root.SetActive(false);
        UIManager.Instance.PlayGameUI.Show(true);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void BtnHomeOnClick()
    {
        SceneManager.LoadScene(GameConstant.BEGIN_SCENE);
        m_root.SetActive(false);
        UIManager.Instance.BeginUI.Show(true);
    }
}
