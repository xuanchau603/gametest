using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;
    
    public HudUI      HudUI;
    public GameOverUI GameOverUI;
    public BeginUI    BeginUI;
    public PlayGameUI PlayGameUI;
    public ListKnifes ListKnifes;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
