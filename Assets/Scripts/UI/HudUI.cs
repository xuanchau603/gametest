using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HudUI : MonoBehaviour
{
    [SerializeField] GameObject m_root;
    [SerializeField] TMP_Text   m_txtApple;

    void OnEnable()
    {
        if (!PlayerPrefs.HasKey("Apple"))
        {
            PlayerPrefs.SetInt("Apple", 0);
        }
        m_txtApple.text = PlayerPrefs.GetInt("Apple").ToString();
    }

    public void Show(bool active)
    {
        m_root.SetActive(active);
    }

    public void UpdateUI()
    {
        m_txtApple.text = PlayerPrefs.GetInt("Apple").ToString();
    }
    
    

}
