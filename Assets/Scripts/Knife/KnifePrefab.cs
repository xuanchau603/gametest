using System;
using System.Linq;
using UnityEngine;

public class KnifePrefab : MonoBehaviour
{
    [SerializeField] Rigidbody2D    m_rigid;
    [SerializeField] float          m_speed;
    [SerializeField] SpriteRenderer m_spriteRenderer;
    [SerializeField] KnifeConfig    m_knifeConfig;

    void Awake()
    {
        //Load skin knife
        if (!PlayerPrefs.HasKey("KnifeSkin"))
        {
            m_spriteRenderer.sprite = m_knifeConfig.ListKnife[0].Image;
            return;
        }
        m_spriteRenderer.sprite = m_knifeConfig.ListKnife.FirstOrDefault(item => item.Id.Equals(PlayerPrefs.GetString("KnifeSkin")))?.Image;
    }

    void FixedUpdate()
    {
        m_rigid.AddForce(Vector2.up * m_speed, ForceMode2D.Impulse);
    }
    
    void OnCollisionEnter2D(Collision2D target)
    {
        if (target.gameObject.CompareTag(GameConstant.PLATE_TAG_NAME))
        {
            transform.SetParent(target.transform);
            m_rigid.bodyType = RigidbodyType2D.Static;
            GameManager.Instance.Score++;
            UIManager.Instance.PlayGameUI.UpdateScore();
        }else if (target.gameObject.CompareTag(GameConstant.KNIFE_TAG_NAME))
        {
            //Lose
            GameManager.Instance.IsGameOver = true;
            UIManager.Instance.PlayGameUI.Show(false);
            UIManager.Instance.GameOverUI.Show(true);
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D target)
    {
        if (target.CompareTag(GameConstant.FRUITS_TAG_NAME))
        {
            int apple = PlayerPrefs.GetInt("Apple");
            PlayerPrefs.SetInt("Apple", apple+2);
            UIManager.Instance.HudUI.UpdateUI();
            Destroy(target.gameObject);
        }
    }
}
