using System;
using System.Linq;
using UnityEngine;

public class SpawnKnife : MonoBehaviour
{
    [SerializeField] GameObject     m_knife;
    [SerializeField] SpriteRenderer m_spriteRenderer;
    [SerializeField] KnifeConfig    m_knifeConfig;

    void Awake()
    {
        //Load skin knife
        if (!PlayerPrefs.HasKey("KnifeSkin"))
        {
            m_spriteRenderer.sprite = m_knifeConfig.ListKnife[0].Image;
            return;
        }
        m_spriteRenderer.sprite = m_knifeConfig.ListKnife.FirstOrDefault(item => item.Id.Equals(PlayerPrefs.GetString("KnifeSkin")))?.Image;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !GameManager.Instance.IsGameOver)
        {
            var go = Instantiate(m_knife, transform.position, Quaternion.identity);
        }
    }
}
