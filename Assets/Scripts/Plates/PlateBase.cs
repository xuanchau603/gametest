using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class PlateBase : MonoBehaviour
{
    [SerializeField] protected GameConfig m_gameConfig;
    protected                  int        point;
    protected                  float      m_speed;
    [SerializeField]           Transform  m_shadowKnifesTrans;
    [SerializeField]           GameObject m_shadowKnife;
    List<GameObject>                      m_listShadowKnife = new List<GameObject>();
    void Start()
    {
        point   = m_gameConfig.ListStage[GameManager.Instance.Stage].AmoutKnife;
        m_speed = m_gameConfig.ListStage[GameManager.Instance.Stage].PlateSpeed;
        for (int i = 0; i < point; i++)
        {
            var go = Instantiate(m_shadowKnife, m_shadowKnifesTrans);
            m_listShadowKnife.Add(go);
        }
    }
    
    void Update()
    {
        if (!GameManager.Instance.IsGameOver)
        {
            Move();
        }
    }
    
    void OnCollisionEnter2D(Collision2D target)
    {
        if (target.gameObject.CompareTag(GameConstant.KNIFE_TAG_NAME))
        {
            UpdateShadowKnife(point);
            point--;
            if (point <= 0)
            {
                if (GameManager.Instance.Stage >= m_gameConfig.ListStage.Count - 1)
                {
                    GameManager.Instance.Stage = 0;
                }
                else
                {
                    GameManager.Instance.Stage++;
                }
                UIManager.Instance.PlayGameUI.UpdateStage();
                Instantiate(m_gameConfig.ListStage[GameManager.Instance.Stage].PlatePrefab, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }
        }
    }

    void UpdateShadowKnife(int index)
    {
        m_listShadowKnife[^index].GetComponent<Image>().color = Color.black;
    }

    protected abstract void Move();

}
