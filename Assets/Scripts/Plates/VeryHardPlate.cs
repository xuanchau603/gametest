using UnityEngine;

public class VeryHardPlate : PlateBase
{
    protected override void Move()
    {
        Quaternion currentRotation = transform.rotation;
        Quaternion newRotation     = Quaternion.Euler(currentRotation.eulerAngles + Vector3.back * m_speed * Time.deltaTime);
        transform.rotation = newRotation;
    }
    
}
