using UnityEngine;

public class MidPlate : PlateBase
{
    void Awake()
    {
        point   = m_gameConfig.ListStage[1].AmoutKnife;
        m_speed = m_gameConfig.ListStage[1].PlateSpeed;
    }
    
    protected override void Move()
    {
        Quaternion currentRotation = transform.rotation;
        Quaternion newRotation     = Quaternion.Euler(currentRotation.eulerAngles + Vector3.back * m_speed * Time.deltaTime);
        transform.rotation = newRotation;
    }
    
}
