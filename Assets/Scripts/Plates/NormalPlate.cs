using System.Collections;
using UnityEngine;

public class NormalPlate : PlateBase
{

    
    protected override void Move()
    {
        Quaternion currentRotation = transform.rotation;
        Quaternion newRotation     = Quaternion.Euler(currentRotation.eulerAngles + Vector3.forward * m_speed * Time.deltaTime);
        transform.rotation = newRotation;
    }
    
    
}
