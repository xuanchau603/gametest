using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HardPlate : PlateBase
{
    protected override void Move()
    {
        Quaternion currentRotation = transform.rotation;
        Quaternion newRotation     = Quaternion.Euler(currentRotation.eulerAngles + Vector3.forward * m_speed * Time.deltaTime);
        transform.rotation = newRotation;
    }
    
}
