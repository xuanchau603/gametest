using UnityEngine;

public class GameConstant 
{
    //tag name
    public static string PLATE_TAG_NAME = "Plate";
    public static string KNIFE_TAG_NAME = "Knife";
    public static string FRUITS_TAG_NAME = "Fruits";

    
    
    //Scene name
    public static string BEGIN_SCENE = "Begin";
    public static string STAGE_SCENE = "Stage";
    
    //Key Save data
    public static string KNIFE_DATA      = "KnifeSave";
    public static string KNIFE_SELECT    = "Knife";
    public static string KNIFE_SKIN = "KnifeSkin";
    public static string APPLE_KEY = "Apple";
    public static string KNIFE_PRICE_KEY = "Price";
    



}
